var Bluejacket = function(scope, options){
    this.scope = scope;
    this.options = options;
    
    this.__appendScope = "bj-";
    this.__mainScope = this.__appendScope + "app";
    
    this.querySelect = document.querySelectorAll(this.__mainScope);

    return this;
};