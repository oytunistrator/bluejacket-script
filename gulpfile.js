var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var browserify = require('gulp-browserify');

gulp.task('default', function() {
    gulp.src(['./src/*.js', './src/lib/*.js'])
        .pipe(browserify({
          insertGlobals : true
        }))
        .pipe(concat('bj.js'))
        .pipe(gulp.dest('./build/'));


    gulp.src(['./src/*.js', './src/lib/*.js'])
        .pipe(browserify({
          insertGlobals : true
        }))
        .pipe(concat('bj.js'))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        .pipe(gulp.dest('./build/'))
        .pipe(gulp.dest('./example/js/'));
});

gulp.task('watch', function() {
    gulp.watch('./lib/*.js', ['default']);
});